const searchInput = document.getElementById('search');
const list = document.getElementById('list');
const copyInput = document.getElementById('copy');

/**
 * request content of file
 * @param url
 * @param callback
 */
function requestJsonFile(url, callback) {
    let xhr = new XMLHttpRequest();
    xhr.onload = function () {
        callback(this.responseText)
    };
    xhr.open('GET', url, true);
    xhr.send();
}

/**
 * get content of json from url
 * @param url
 * @return {Promise<any>}
 */
function getJsonContent(url) {
    return new Promise((resolve) => {
        requestJsonFile(url, data => resolve(JSON.parse(data)));
    });
}

/**
 * returns array of indexes ab => [a, ab]
 * @param word
 * @return {[]}
 */
function getIndexes(word) {
    word = word.toLowerCase();
    let indexes = [];
    for (let i = 0; i < word.length; i++) {
        for (let len = 1; len <= (word.length - i); len++){
            indexes.push(word.substr(i, len));
        }
    }
    return indexes;
}

/**
 * render item to page
 * @param code
 * @param b64
 */
function render(code, b64) {
    const listItem = document.createElement('button');
    listItem.innerHTML = `<img src="${b64}" alt="${code}"><br/><span>${code}</span>`;

    // copy emote code function
    listItem.addEventListener('click', event => {
        copyInput.value = code;
        copyInput.select();
        document.execCommand("copy");
        copyInput.value = '';
    })
    list.appendChild(listItem);
}

/**
 * creates map of indexes for search
 * @param emoteList
 * @return {Map<any, any>}
 */
function getIndexMap(emoteList) {
    const indexMap = new Map();

    for(const emoteCode in emoteList) {
        for ( const index of getIndexes(emoteCode) ) {
            if ( indexMap.has(index) ) {
                let obj = indexMap.get(index);
                obj[emoteCode] = emoteList[emoteCode];
                indexMap.set(index, obj)
            } else {
                indexMap.set(index, {
                    [emoteCode]: emoteList[emoteCode]
                });
            }
        }
    }

    return indexMap;
}

(async function main() {
    list.innerText = 'Loading data...';

    const data = await getJsonContent('emotes.json');

    list.innerText = 'Indexing emotes...';
    const indexMap = getIndexMap(data.emoteList);
    indexMap.set('', data.emoteList); // for empty search return all emotes

    list.innerText = '';
    for (const emoteCode of Object.keys(data.emoteList)) {
        render(emoteCode, data.emoteList[emoteCode]);
    }

    searchInput.addEventListener('keyup', event => {
        list.innerHTML = '';
        const searchString = event.target.value.toLowerCase();
        if (indexMap.has(searchString)) {
            const subList = indexMap.get(searchString);
            for (const emoteCode of Object.keys(subList)) {
                render(emoteCode, subList[emoteCode]);
            }
        } else {
            list.innerHTML = '';
            list.innerText = 'Not found';
        }
    });
})();
